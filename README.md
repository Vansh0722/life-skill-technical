# TLA+ #

![](https://upload.wikimedia.org/wikipedia/en/6/63/TLA%2B_logo_splash_image.png)

## Abstracts:
---
-  TLA+ is a high-level language for modeling programs and systems--especially concurrent and distributed ones.  It's based on the idea that the best way to describe things precisely is with simple mathematics.  TLA+ and its tools are useful for eliminating fundamental design errors, which are hard to find and expensive to correct in code.

---
## Table of Contents :
---

  1. [Introduction](#introduction)
  
  1. [Operators](#Operators)

  1. [Standard modules](#Standard-modules)

  1. [Tools](#Tools)
     1. [Use cases:](#use-cases)

          - [IDE](#1IDE)

          - [Model checker](#2Model-checker)

          - [Proof System](#3Proof-System)

  1. [Industry Use](#Industry-Use)

 

---
  ## Introduction 
 ---
 - TLA+ was developed by computer scientist and 2013 Turing award recipient Leslie Lamport.

 - TLA+ is a formal specification language.
 
 - It’s a tool to design systems and algorithms, then programmatically verify that those systems don’t have critical bugs. 
     
- IIt’s the software equivalent of a blueprint.
 ---
 ## Operators      
 ---

- TLA+ is based on ZF, so operations on variables involve set manipulation. The language includes set membership, union, intersection, difference, powerset, and subset operators.

- First-order logic operators such as ∨, ∧, ¬, ⇒, ↔, ≡ are also included, as well as universal and existential quantifiers ∀ and ∃.

- Hilbert's ε is provided as the CHOOSE operator, which uniquely selects an arbitrary set element.

-  Arithmetic operators over reals, integers, and natural numbers are available from the standard modules. 

---
## Standard modules
---
- ### FiniteSets:
 Module for working with finite sets. Provides IsFiniteSet(S) and Cardinality(S) operators.

- ### Sequences: 
Defines operators on tuples such as Len(S), Head(S), Tail(S), Append(S, E), concatenation, and filter.

- ### Bags: 
Module for working with multisets. Provides primitive set operation analogues and duplicate counting.

---
### Tools
---
### 1. IDE
---

- An integrated development environment is implemented on top of Eclipse. It includes an editor with error and syntax highlighting, plus a GUI front-end to several other TLA+ tools 

- The SANY syntactic analyzer, which parses and checks the spec for syntax errors.

- The LaTeX translator, to generate pretty-printed specs.

- The PlusCal translator, The TLC model checker and The TLAPS proof system.

---
 ### 2.Model checker
---
- The TLC model checker builds a finite state model of TLA+ specifications for checking invariance properties.

- TLC generates a set of initial states satisfying the spec, then performs a breadth-first search over all defined state transitions.

- Execution stops when all state transitions lead to states which have already been discovered.

- it halts and provides a state trace path to the offending state. TLC provides a method of declaring model symmetries to defend against combinatorial explosion


![Graphql Layer](https://upload.wikimedia.org/wikipedia/commons/3/3d/TLC_one-bit_clock_states.png)

---
### 3.Proof System
---

- The TLA+ Proof System, or TLAPS, mechanically checks proofs written in TLA+. It was developed at the Microsoft Research-INRIA Joint Centre to prove correctness of concurrent and distributed algorithms. 

-  The proof language is designed to be independent of any particular theorem prover; proofs are written in a declarative style, and transformed into individual obligations which are sent to back-end provers.

- The primary back-end provers are Isabelle and Zenon, with fallback to SMT solvers CVC3, Yices, and Z3.  


---
## Industry Use

---
- At Microsoft, a critical bug was discovered in the Xbox 360 memory module during the process of writing a specification in TLA+.[24] TLA+ was used to write formal proofs of correctness for Byzantine Paxos and components of the Pastry distributed hash table.

- Amazon Web Services has used TLA+ since 2011. TLA+ model checking uncovered bugs in DynamoDB, S3, EBS, and an internal distributed lock manager; some bugs required state traces of 35 steps. Model checking was also used to verify aggressive optimizations. In addition, TLA+ specifications were found to hold value as documentation and design aids.

- Microsoft Azure used TLA+ to design Cosmos DB, a globally-distributed database with five different consistency models.

---

## References :

1. [Tutorialspoint](https://www.youtube.com/results?search_query=tla%2B+tutorial) 

1. [TLA+ Home](http://lamport.azurewebsites.net/tla/tla.html)


